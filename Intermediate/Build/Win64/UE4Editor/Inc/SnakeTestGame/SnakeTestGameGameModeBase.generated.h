// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKETESTGAME_SnakeTestGameGameModeBase_generated_h
#error "SnakeTestGameGameModeBase.generated.h already included, missing '#pragma once' in SnakeTestGameGameModeBase.h"
#endif
#define SNAKETESTGAME_SnakeTestGameGameModeBase_generated_h

#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_SPARSE_DATA
#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_RPC_WRAPPERS
#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeTestGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeTestGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeTestGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeTestGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeTestGameGameModeBase)


#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeTestGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeTestGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeTestGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeTestGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeTestGameGameModeBase)


#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeTestGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeTestGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeTestGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeTestGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeTestGameGameModeBase(ASnakeTestGameGameModeBase&&); \
	NO_API ASnakeTestGameGameModeBase(const ASnakeTestGameGameModeBase&); \
public:


#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeTestGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeTestGameGameModeBase(ASnakeTestGameGameModeBase&&); \
	NO_API ASnakeTestGameGameModeBase(const ASnakeTestGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeTestGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeTestGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeTestGameGameModeBase)


#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_12_PROLOG
#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_SPARSE_DATA \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_RPC_WRAPPERS \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_INCLASS \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_SPARSE_DATA \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKETESTGAME_API UClass* StaticClass<class ASnakeTestGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeTestGame_Source_SnakeTestGame_SnakeTestGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
