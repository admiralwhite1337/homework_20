// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeTestGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKETESTGAME_API ASnakeTestGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
