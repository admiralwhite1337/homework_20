// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeTestGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeTestGame, "SnakeTestGame" );
